package com.example.weatherapp.pojo;

import java.util.List;

/**
 * Created by ximik on 16.04.17.
 */

public class Forecast {
    private Long dt;
    private Temperature temp;
    private Float pressure,
            speed,
            rain;
    private List<Weather> weather;
    private Integer humidity,
            deg,
            clouds;

    //region Getters
    public Long getDt() {
        return dt;
    }

    public Temperature getTemp() {
        return temp;
    }

    public Float getPressure() {
        return pressure;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public Float getSpeed() {
        return speed;
    }

    public Integer getDeg() {
        return deg;
    }

    public Integer getClouds() {
        return clouds;
    }

    public Float getRain() {
        return rain;
    }
    //endregion


    //region Setters
    public void setDt(Long dt) {
        this.dt = dt;
    }

    public void setTemp(Temperature temp) {
        this.temp = temp;
    }

    public void setPressure(Float pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public void setDeg(Integer deg) {
        this.deg = deg;
    }

    public void setClouds(Integer clouds) {
        this.clouds = clouds;
    }

    public void setRain(Float rain) {
        this.rain = rain;
    }
    //endregion
}
