package com.example.weatherapp.pojo;

/**
 * Created by ximik on 16.04.17.
 */

public class Weather {
    private Integer id;
    private String main,
            description,
            icon;

    //region Getters
    public Integer getId() {
        return id;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }
    //endregion

    //region Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    //endregion
}
