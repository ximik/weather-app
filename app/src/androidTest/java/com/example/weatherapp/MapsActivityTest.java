package com.example.weatherapp;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.weatherapp.pojo.Forecast;
import com.example.weatherapp.pojo.Weather;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * Created by ximik on 16.04.17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MapsActivityTest {
    static final String TEST = "test";
    @Rule
    public ActivityTestRule<MapsActivity> rule = new ActivityTestRule<>(MapsActivity.class);

    @Test
    public void checkListBinding() throws InterruptedException {
        Weather weather = new Weather();
        weather.setMain(TEST);
        Forecast forecast = new Forecast();
        forecast.setWeather(singletonList(weather));
        Forecast[] forecasts = { forecast, forecast, forecast };

        rule.getActivity().binding.setForecasts(asList(forecasts));
        Thread.sleep(800);
        onView(withRecyclerView(R.id.design_bottom_sheet).atPosition(1))
                .check(matches(hasDescendant(withText(TEST))));
    }

    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }
}
