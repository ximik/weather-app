package com.example.weatherapp.pojo;

/**
 * Created by volodymyr.kukhar on 4/24/17.
 */

public class City {
    Integer id;
    String name, country;

    //region Getters
    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }
    //endregion

    //region Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    //endregion
}
