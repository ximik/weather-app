package com.example.weatherapp.pojo;

import java.util.List;

/**
 * Created by ximik on 16.04.17.
 */

public class WeatherResponse {
    String cod;
    City city;
    Float message;
    List<Forecast> list;

    public String getCod() {
        return cod;
    }

    public City getCity() {
        return city;
    }

    public Float getMessage() {
        return message;
    }

    public List<Forecast> getList() {
        return list;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setMessage(Float message) {
        this.message = message;
    }

    public void setList(List<Forecast> list) {
        this.list = list;
    }
}
