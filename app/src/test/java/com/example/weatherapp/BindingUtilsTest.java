package com.example.weatherapp;

import android.text.format.DateUtils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by ximik on 23.04.17.
 */
public class BindingUtilsTest {
    @Test
    public void getDay() throws Exception {
        // Epoch day was Thursday
        assertThat(BindingUtils.getDay(0), is("Thursday"));

        // Fails due to android DateUtils.isToday()
        assertThat(BindingUtils.getDay(System.currentTimeMillis() / 1000), is("Today"));
    }
}