package com.example.weatherapp.pojo;

/**
 * Created by ximik on 16.04.17.
 */

public class Temperature {
    private Float day, min, max, night, eve, morn;

    //region Getters
    public float getDay() {
        return day;
    }

    public float getMin() {
        return min;
    }

    public float getMax() {
        return max;
    }

    public float getNight() {
        return night;
    }

    public float getEve() {
        return eve;
    }

    public float getMorn() {
        return morn;
    }
    //endregion

    //region Setters
    public void setDay(float day) {
        this.day = day;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public void setNight(float night) {
        this.night = night;
    }

    public void setEve(float eve) {
        this.eve = eve;
    }

    public void setMorn(float morn) {
        this.morn = morn;
    }
    //endregion
}
