package com.example.weatherapp;

import android.databinding.BindingAdapter;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;

/**
 * Created by ximik on 09.04.17.
 */

public class BindingUtils {
    @BindingAdapter("layoutManager")
    public static void setLayoutManager(final RecyclerView recyclerView, @LinearLayoutCompat.OrientationMode int type) {
        final LinearLayoutManager manager;
        switch (type) {
            case LinearLayout.HORIZONTAL:
                manager = new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false);
                break;
            case LinearLayout.VERTICAL:
                manager = new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.VERTICAL, false);
                break;
            default:
                manager = new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.VERTICAL, false);
                break;
        }
        recyclerView.setLayoutManager(manager);
    }

    @BindingAdapter({"rowLayout", "items"})
    public static <T> void setRowLayout(RecyclerView recyclerView, @LayoutRes int rowLayout, @Nullable List<T> items) {
        if (recyclerView.getAdapter() != null
                && !(recyclerView.getAdapter() instanceof BindableAdapter)) {
            Log.e("RecyclerView", "Current adapter is not bindable. Skipping binding layout");
            return;
        } else if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(new BindableAdapter(rowLayout));

        BindableAdapter adapter = (BindableAdapter) recyclerView.getAdapter();
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @BindingAdapter("url")
    public static void setImageUrl(ImageView imageView, String imageUrl) {
        Picasso.with(imageView.getContext()).load(imageUrl).into(imageView);
    }

    public static String getDay(long millis) {
        if (DateUtils.isToday(millis)) return "Today";
        if (millis < System.currentTimeMillis())
            return DateUtils.getRelativeTimeSpanString(millis).toString();

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millis);
        return DateFormatSymbols.getInstance().getWeekdays()[c.get(Calendar.DAY_OF_WEEK)];
    }

}