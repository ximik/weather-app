package com.example.weatherapp;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ximik on 09.04.17.
 */

public class BindableAdapter extends RecyclerView.Adapter<BindableAdapter.BindingViewHolder> {
    private @LayoutRes
    int itemLayoutId;
    private ArrayList<Object> items = new ArrayList<>();

    public BindableAdapter(@LayoutRes int itemLayoutId) {
        this.itemLayoutId = itemLayoutId;
    }

    public <T> void setItems(@Nullable List<T> items) {
        if (items != null) {
            this.items.clear();
            this.items.addAll(items);
        }
    }

    public ArrayList<Object> getItems() {
        return items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BindingViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.getContext()),
                        itemLayoutId,
                        parent,
                        false)
        );
    }


    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    static class BindingViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        BindingViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        public void bind(Object object) {
            binding.setVariable(BR.object, object);
            binding.executePendingBindings();
        }
    }
}
