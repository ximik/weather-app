package com.example.weatherapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.weatherapp.databinding.MapsActivityBinding;
import com.example.weatherapp.pojo.WeatherResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    static final int COARSE_LOCATION_PERMISSION = 13465;
    static final String BASE_URL = "http://api.openweathermap.org/";

    CompositeDisposable cd = new CompositeDisposable();
    MapsActivityBinding binding;
    GoogleMap map;
    Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.maps_activity);
        api = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .addInterceptor(chain -> {
                            Request origin = chain.request();
                            return chain.proceed(origin.newBuilder().url(
                                    origin.url().newBuilder()
                                            .addQueryParameter("APPID", getString(R.string.app_id))
                                            .addQueryParameter("units", "metric")
                                            .addQueryParameter("cnt", "10")
                                            .build())
                                    .build());
                        })
                        .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .build()
                )
                .build()
                .create(Api.class);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Get current location weather
        handleResponse(weatherCurrentRequestMap());
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapLongClickListener(latLng -> {
            map.clear();
            map.addMarker(new MarkerOptions().position(latLng));
            handleResponse(weatherByLocationRequestMap(latLng.latitude, latLng.longitude));
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cd.dispose();
    }

    public Boolean onSearch(TextView v) {
        // More hacky than expected
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
        handleResponse(weatherBySearchRequestMap(v.getText().toString()));
        return true;
    }


    public interface Api {
        @GET("data/2.5/forecast/daily")
        Single<Response<WeatherResponse>> weather(@QueryMap Map<String, String> params);
    }

    void handleResponse(Single<Map<String, String>> requestParams) {
        cd.add(requestParams.observeOn(Schedulers.io())
                .flatMap(api::weather)
                .flatMap(r -> r.code() == 200
                        ? Single.just(r)
                        : Single.error(new Throwable(r.errorBody().string())))
                .map(Response::body)
                .flatMap(r -> "200".equals(r.getCod())
                        ? Single.just(r)
                        : Single.error(new Throwable(getString(R.string.server_resp_with, r.getCod()))))
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterSuccess(wr -> binding.searchField.setText(wr.getCity().getName()))
                .map(WeatherResponse::getList)
                .subscribe(binding::setForecasts, this::showError)
        );
    }

    Single<Map<String, String>> weatherCurrentRequestMap() {
        return checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnError(t -> ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        COARSE_LOCATION_PERMISSION))
                .andThen(getLastKnownLocation())
                .map(location -> new Double[]{location.getLatitude(), location.getLongitude()})
                .doAfterSuccess(latLon -> map.animateCamera(
                        CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(
                                new LatLng(latLon[0], latLon[1]), 10))))
                .flatMap(this::weatherByLocationRequestMap)
                ;
    }

    Single<Map<String, String>> weatherByLocationRequestMap(Double... latLong) {
        return Single.just(latLong)
                .map(l -> new HashMap<String, String>(2) {{
                    put("lat", String.valueOf(l[0]));
                    put("lon", String.valueOf(l[1]));
                }})
                ;
    }

    Single<Map<String, String>> weatherBySearchRequestMap(String q) {
        return Single.just(new HashMap<String, String>(1) {{
            put("q", q);
        }})
                ;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == COARSE_LOCATION_PERMISSION)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                handleResponse(weatherCurrentRequestMap());
            else
                showError(new Throwable(getString(R.string.not_granted, permissions[0])));
    }

    Completable checkPermission(@NonNull String permission) {
        return Completable.create(e -> {
            if (ContextCompat.checkSelfPermission(this, permission)
                    == PackageManager.PERMISSION_GRANTED)
                e.onComplete();
            else
                e.onError(new SecurityException(getString(R.string.not_granted, permission)));
        });
    }

    Single<Location> getLastKnownLocation() {
        return Single.create(e -> {
            try {
                Location location = ((LocationManager) getSystemService(LOCATION_SERVICE))
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) e.onSuccess(location);
                else e.onError(new Throwable(getString(R.string.location_not_available)));
            } catch (SecurityException x) {
                e.onError(x);
            }
        });
    }

    void showError(Throwable e) {
        Snackbar.make(binding.root, e.getMessage(), Snackbar.LENGTH_LONG).show();
    }
}
